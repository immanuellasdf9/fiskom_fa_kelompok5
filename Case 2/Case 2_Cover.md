# **CASE 2 FISIKA KOMPUTASI**

**<h2>Kelompok 5</h2>**

**<h2>Anggota Kelompok</h2>**
<ul>
    <li>Immanuella Senja Dwi Febriani (NRM: 1306620006)</li>
	<li>Hernanda Khoiriyah Putri (NRM: 1306620025)</li>
	<li>Siva Ardelia Azzahra (NRM: 1306620030)</li>
	<li>Kunti Dewanti (NRM: 1306620026)</li>
</ul>

**<h2>Tema Project</h2>**
<p>Electrostatic Force System using Newton Interpolation and Outline Image using Cubic Spline</p>

**<h2>Video Presentasi</h2>**
<p>Youtube : https://youtu.be/vxcFnqoa47E</p>